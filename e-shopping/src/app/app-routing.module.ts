import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginSignupComponent } from './Component/login-signup/login-signup.component';
import { LoginComponent } from './Component/login-signup/login-signup-comp/login/login.component';
import { SignupComponent } from './Component/login-signup/login-signup-comp/signup/signup.component';
import { PrivateRouterComponent } from './Component/private-router/private-router.component';
import { AdminComponent } from './Component/private-router/private-rout-comp/admin/admin.component';
import { UserComponent } from './Component/private-router/private-rout-comp/user/user.component';

const routes: Routes = [
  {path:'home',component:PrivateRouterComponent ,children:[
      {path:'admin',component:AdminComponent},
      {path:'user',component:UserComponent}    
    ]
  },
  {path:'',component:LoginSignupComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingConponent =[
  LoginSignupComponent, 
  LoginComponent, 
  SignupComponent, 
  PrivateRouterComponent, 
  AdminComponent, 
  UserComponent
]
