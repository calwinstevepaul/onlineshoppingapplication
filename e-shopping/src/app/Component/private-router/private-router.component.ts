import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {AppComponent} from '../../app.component'

@Component({
  selector: 'app-private-router',
  templateUrl: './private-router.component.html',
  styleUrls: ['./private-router.component.css', '../login-signup/login-signup-comp/login/login.component.css']
})
export class PrivateRouterComponent implements OnInit {

  constructor(private _AppComponent:AppComponent, private router:Router ) { }

  isAdmin = this._AppComponent.isAdmin
  isLogin = this._AppComponent.isLogin
  
  ngOnInit(): void {
    if(this.isAdmin && this.isLogin){
      this.router.navigate(['/home/admin'])
    }
    else if(this.isLogin){
      this.router.navigate(['/home/user'])
    }
    else{
      this.router.navigate(['/'])

    }
  }

  logout(){
    this._AppComponent.changeAdmin(false)
    this._AppComponent.changeLogin(false)
    localStorage.clear()
    this.router.navigate(['/'])

  }



}
