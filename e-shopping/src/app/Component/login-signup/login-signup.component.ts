import { Component, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-login-signup',
  templateUrl: './login-signup.component.html',
  styleUrls: ['./login-signup.component.css']
})
export class LoginSignupComponent implements OnInit {
  
  isLogin:boolean =true

  constructor() {  }

  ngOnInit(): void {
  }

  changeIsLogin() {
    this.isLogin= ! this.isLogin
  }

}
