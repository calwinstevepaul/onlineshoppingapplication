import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../../../Services/auth/auth.service'
import {AppComponent} from '../../../../app.component'
import { Router } from '@angular/router';
import { async } from 'rxjs/internal/scheduler/async';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user = {
    email: "" ,
    password: ""

  }

  constructor( private _login:AuthService, private router:Router, private _AppComponent:AppComponent ) { }

  ngOnInit(): void {
  }

  logdetails:any={}

  submit(){
    this._login.loginUser(this.user)
    .subscribe(
      (data)=>{
        this.logdetails=data
        if(this.logdetails.status === "successful"){
          localStorage.setItem("token",JSON.stringify(this.logdetails.token))
          this._AppComponent.changeLogin(true)
          console.log(this.logdetails.isAdmin)
          this._AppComponent.changeAdmin(this.logdetails.isAdmin)
          this.router.navigate(['/home'])
        }
      },
      (err)=>{
        alert(err.error.status)
      }
    )
    
  }
  
}
