import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../../../Services/auth/auth.service'
import axios from 'axios';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css','../login/login.component.css']
})
export class SignupComponent implements OnInit {
 user= {
   name:"", 
   email:"",
   address:"",
   phone:"",
   password:""
 }

  constructor(private _signup:AuthService) { }

  ngOnInit(): void {
  } 

  submit(){
    this._signup.signupUser(this.user)
    .subscribe(
      (data)=>{
      console.log(data)
      alert("You have been Successfully signed up")
      },
      (err)=>{
        console.log(err)
        alert(err.statusText)
      }
    )
    
  }

  

}
