import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms'
import{HttpClientModule } from '@angular/common/http'


import { AppRoutingModule, routingConponent } from './app-routing.module';
import { AppComponent } from './app.component';
import {AuthService} from './Services/auth/auth.service';



@NgModule({
  declarations: [
    AppComponent,
    routingConponent   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
    
  ],
  providers: [AuthService, AppComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
