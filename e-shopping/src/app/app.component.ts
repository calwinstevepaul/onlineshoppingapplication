import { Component } from '@angular/core';
import {AuthService} from './Services/auth/auth.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'e-shoping';
  isLogin = localStorage.getItem('token')?true:false;
  isAdmin = false
  user:any = {}

  constructor( private _login:AuthService ) { }

  changeLogin (data){
    this.isLogin = data
  } 

  changeAdmin (data){
    this.isAdmin = data
  }
}
 