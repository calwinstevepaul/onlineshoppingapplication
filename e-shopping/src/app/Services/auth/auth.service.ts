import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http:HttpClient) { }

  loginUser=(details)=>{
    return this.http.post('http://localhost:9000/auth/login',details)
  }

  signupUser=(details)=>{
    return this.http.post('http://localhost:9000/auth/signup',details)
  }

  isAdmin=(details)=>{
    return this.http.post('http://localhost:9000/auth/user',details)
  }

}
