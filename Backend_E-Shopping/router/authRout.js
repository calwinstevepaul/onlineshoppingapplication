const router = require("express").Router();
const bcrypt = require('bcrypt')
const jwt = require("jsonwebtoken");
const middlewere = require ('../middleware/middleware')



class auth {

  constructor(authController) {
    this.controller = authController
    this.init();
  }
 
  init() {
    router.post('/signup', (req, res) => {
        const {name, email, password, address, phone} = req.body
        this.controller.signup(name, email, password, address, phone)
            .then(data=>{
                
                res.send(data)
            })
            .catch(e=>{
                res.status(400).send(e.message)
                
            })
    })
    
    router.post('/login', (req, res)=>{
        const {email, password} = req.body
        console.log(req.body)

        this.controller.login(email)
        .then(data=>{
           if(data){
                bcrypt.compare(password, data.password, function(err, result) {
                    if(result){
                        const token = jwt.sign(
                            { id: data },"calwin123",{ expiresIn: "3h" }
                        );
                        res.send({
                            status:"successful",
                            login:true,
                            userId:data._id,
                            isAdmin:data.isAdmin,
                            token
                        })
                    }
                    else{
                        res.status(400).send({
                            status:"Wrong Password",
                            login:false,
                            userId:data._id
                        })
                    }
                });
           }
           else{
            res.status(400).send({
                status:"Invalid Email",
                login:false
            })
           }
        })
        .catch(e=>{
            res.status(400).send(e.message)
        })
    })


    router.post('/user',middlewere,(req,res)=>{
        const userId = req.user
        console.log("router",userId)
        this.controller.isAdmin(userId).then(data=>{
            res.send(data)
        })
    })
    
  }

  getRouter() {
    return router;
  }
}

module.exports = controller => {
  return new auth(controller);
};
