var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const index = { name: 'text', email: 'text' };

var users = new Schema({
    name:String,
    email:{type:String,index:{unique:true}},
    password:String,
    address:String,
    phone:String,
    isAdmin:{type:Boolean,default:false},
    last_update:{type:Date,default:new Date()}
  });

  users.index(index);

var Users = mongoose.model('users',users);
module.exports = Users; 