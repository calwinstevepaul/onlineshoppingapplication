const bcrypt = require('bcrypt')
const User = require("../models/User")


class authController {
    async signup (name, email, password, address, phone){
        const hash = await bcrypt.hashSync(password, 10);

        return User.create({
            name: name,
            email:email,
            address:address, 
            phone:phone,
            password:hash
        })
    }

    async login (email, password){
        return User.findOne({
            email: email
        })
    }

    async isAdmin(id) {
        const user = User.findById(id, 'isAdmin -_id')
        return user
    }
   

}


module.exports = () => {
    return new authController();
};
