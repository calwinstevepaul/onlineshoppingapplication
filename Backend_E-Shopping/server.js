const express = require('express')
const app = express();
const cors = require("cors");
const mongoose = require("mongoose");



mongoose.connect('mongodb://localhost/e_shoping')
.then(()=>console.log("Connected to DB"))
.catch(()=>console.error("Not connect to db"))

//controllers
const authController = require("./controller/authController")();



//routes
const authRoute = require("./router/authRout")(authController);


app.use(express.json());
app.use(cors())
app.use("/auth", authRoute.getRouter());

app.listen(9000, () => {
    console.log("listening................ 9000")
  });
  
  module.exports = app