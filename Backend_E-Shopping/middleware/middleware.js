var jwt = require("jsonwebtoken");


module.exports = (req, res, next) => {
    var token = req.body.token
    jwt.verify(token, "calwin123", async (err, decode) => {
        if (err) {
            res.status(400).send({
                message: "Invalid token"
            }) 
        }
        else {
            req.user = decode.id._id
            next() 
        }
    })
}